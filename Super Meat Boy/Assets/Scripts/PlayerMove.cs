﻿﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{

    public float moveSpeed;
    public float jumpHeight;
    public Transform jumpChecked;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private bool grounded;
    private Rigidbody2D rb2d;
    float jumpTime;
    float jumpDelay = .7f;
    bool jumped;
    private Animator anim;
    private bool muerto = false;
    private Vector3 origen;
    private AudioSource sound;

    // Use this for initialization
    void Awake()
    {
        sound = GetComponent<AudioSource>();
        origen = this.transform.position;

        anim = GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        if (muerto)
        {
            Restart();
        }

        //grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }
    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        anim.SetFloat("speed", Mathf.Abs(horizontal));

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space) || grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
            jumpTime = jumpDelay;
            anim.SetTrigger("Jump");
            jumped = true;
            grounded = false;
            sound.Play();
        }
        else
        {
            anim.SetTrigger("Land");
        }


        jumpTime -= Time.deltaTime;
        if (jumpTime <= 0 || grounded == true || jumped)
        {
            anim.SetTrigger("Land");
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "muerte")
        {
            muerto = true;
            anim.SetTrigger("Die");
            

        }
        else
        {
            muerto = false;
            anim.SetTrigger("Land");
        }
    }

    void Restart()
    {
        this.transform.position = origen;
        muerto = false;
    }
} 

